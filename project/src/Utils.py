import json
import os
import sys
import traceback

import pygame
from pygame.locals import RLEACCEL


def load_image(img_path, colorkey=None):
    try:
        image = pygame.image.load(img_path)
    except pygame.error:
        print(f"Cannot load image: {img_path}")
        traceback.print_exc(file=sys.stdout)

    image = image.convert()
    if colorkey is not None:
        if colorkey is -1:
            colorkey = image.get_at((0, 0))
        image.set_colorkey(colorkey, RLEACCEL)

    return image, image.get_rect()


def load_sound(name, music=False):
    class NoneSound:
        def play(self):
            pass
    if not pygame.mixer:
        return NoneSound()
    fullname = os.path.join('project', 'res', 'assets', 'audio', name)
    try:
        if music:
            sound = pygame.mixer.music.load(fullname)
        else:
            sound = pygame.mixer.Sound(fullname)
    except pygame.error:
        print(f"Cannot laod sound: {name}")
        print(f"{os.getcwd()}")
    return sound


def load_config(path):
    with open(path) as file:
        return json.load(file)
