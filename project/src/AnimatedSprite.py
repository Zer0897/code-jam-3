import os

import pygame
from pygame.sprite import groupcollide


class AnimatedSpriteGroup(pygame.sprite.Group):

    def __init__(self, room, level_group=None, player_group=None, debug=False):
        pygame.sprite.Group.__init__(self)
        self.debug = debug
        self.level_group = level_group
        self.player_group = player_group
        self.room = room

    def handle_input(self, input_component):
        for sprite in self.sprites():
            sprite.handle_input(input_component)

    def set_level_group(self, group):
        self.level_group = group

    def set_player_group(self, group):
        self.player_group = group

    def draw(self, surface):
        for sprite in self.sprites():
            sprite.draw(surface)

    def clear_sprites(self):
        for sprite in self.sprites():
            sprite.kill()

    def handle_triggers(self):
        for sprite in self.sprites():
            sprite.handle_triggers()

    def handle_collision(self, sprite_group):
        # Handle Room collision
        room_rects = [self.room.northw.get_rect(),  # NOQA
                      self.room.southw.get_rect(),
                      self.room.eastw.get_rect(),
                      self.room.westw.get_rect()]

        top_rect = pygame.Rect((0, 0), (2000, 1))
        right_rect = pygame.Rect(self.room.right, self.room.eastw.get_rect().size)
        bottom_rect = pygame.Rect(self.room.bottom, self.room.southw.get_rect().size)
        left_rect = pygame.Rect(self.room.left, self.room.westw.get_rect().size)
        for sprite in self.sprites():
            sprite_hurtbox = sprite.get_hurtbox()
            if sprite_hurtbox.colliderect(top_rect):
                sprite.rect.centery += 5
            elif sprite_hurtbox.colliderect(left_rect):
                sprite.rect.centerx += 5
            elif sprite_hurtbox.colliderect(bottom_rect):
                sprite.rect.centery -= 5
            elif sprite_hurtbox.colliderect(right_rect):
                sprite.rect.centerx -= 5

        # Test for all other collisions
        collisions = groupcollide(self, sprite_group, False, False)
        if collisions:
            if self.debug:
                pass  # print(f"Collisions: {collisions}")
            for src_obj in collisions:
                src_obj.handle_collision(collisions[src_obj])


class AnimatedSprite(pygame.sprite.Sprite):

    SUPPORTED_FILETYPES = ['gif']
    ASSET_DIR = os.path.join('project', 'res', 'assets', 'sprites')

    def __init__(self, position=None, level_assets=None, players=None, debug=False):
        pygame.sprite.Sprite.__init__(self)
        self.debug = debug
        self.state = []
        self.animations = {}
        self.level_assets = level_assets
        self.players = players
        self.damage = 0
        self.current_animation = None
        self.animation_stuck = 0
        self.pop_when_complete = False  # Pop the state after the current animation has finished?
        position = position if position else [0, 0]
        self.rect = pygame.Rect(position, (0, 0))

    def take_damage(self):
        pass

    def handle_input(self, input_component):
        pass

    def pop_state(self):
        state = None
        if len(self.state) > 1:  # Ensure that the initial state doesn't get popped off the stack
            state = self.state.pop()
            if self.debug:
                print(f"Popped state: {state}")
        if state:
            state.exit(self)
        self.state[-1].enter(self)
        if self.debug:
            print(f"State Stack: {self.state}")

    def deals_damage(self):
        return self.damage > 0

    def set_animation(self, state, name):
        pass

    def handle_collision(self, collisions):
        pass

    def draw(self, surface):
        if self.current_animation:
            # Check if animation is stuck and pop state if it is
            # What-a-PITA.... if only PygAnimation.isFinished() worked as intended....
            if not self.current_animation.loop and self.current_animation.elapsed == 0:
                self.animation_stuck += 1
            if self.state and self.animation_stuck >= 1:
                self.animation_stuck = 0
                self.pop_state()

            # Blit Animation
            self.current_animation.blit(surface, (self.rect.x, self.rect.y))
