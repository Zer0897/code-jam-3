import os
from random import randint

import pygame

from .NPC.Golem import GolemSprite
from .NPC.Wizard import WizardSprite
from .Utils import load_sound


class GameState:

    # Overlay render positions
    OVERLAY_POINTS = (300, 5)
    OVERLAY_HEALTH = (100, 5)
    OVERLAY_WAVE = (935, 5)
    OVERLAY_TIME = (1055, 5)
    NUM_WAVES = 10

    def __init__(self, player, npc_group, areas, debug=False):
        self.points = 0
        self.wave = 1
        self.countdown = 600  # 10 Minutes
        self.game_timer = self.countdown
        self.start_time = 0
        self.font = pygame.font.Font(None, 32)
        self.debug = debug
        self.game_started = False
        self.init_overlay()

        self.spawn_points = [pygame.Rect((100, 0), (0, 0)),
                             pygame.Rect((350, 0), (0, 0)),
                             pygame.Rect((550, 0), (0, 0)),
                             pygame.Rect((750, 0), (0, 0)),
                             pygame.Rect((1000, 0), (0, 0))]

        self.player = player
        self.npc_group = npc_group
        self.areas = areas
        self.arena = self.areas.get('arena')
        self.start = self.areas.get('start')

        self.background_music = load_sound('dark_ambience_loop.ogg', music=True)
        pygame.mixer.music.play(-1)
        pygame.mixer.music.set_volume(0.15)
        self.start_dialogue = load_sound('intro good take.ogg')
        self.start_dialogue.play()
        self.win_sound_effect = load_sound(os.path.join('Player', 'player_celebration_1.ogg'))
        self.played_win_effect = False

        self.spawn_switch = True

    def update_game_timer(self):
        pass

    def start_game(self):
        self.start_time = pygame.time.get_ticks()
        self.player.rect.centerx = 600
        self.player.rect.centery = 600
        self.game_started = True

    def spawn_wizard(self, position):
        self.npc_group.add(WizardSprite(self.player, position))

    def spawn_enemy(self, position):
        self.npc_group.add(GolemSprite(self.player, position.topleft, self.debug, self.point_callback))

    def point_callback(self):
        self.points += 100

    def init_overlay(self):
        self.point_text = self.font.render("Points: ", 1, (255, 255, 255))
        self.health_text = self.font.render("Health: 100", 1, (255, 255, 255))
        self.wave_text = self.font.render("Wave: 10", 1, (255, 255, 255))
        self.time_text = self.font.render("Time: 1:00", 1, (255, 255, 255))

    def player_has_won(self):
        if self.game_timer <= 0:
            if not self.played_win_effect:
                self.win_sound_effect.play()
                self.played_win_effect = True
            return True

    def update_overlay(self):
        self.point_text = self.font.render(f"Points: {self.points:013d}", 1, (255, 255, 255))
        self.health_text = self.font.render(f"Health: {self.player.health}", 1, (255, 255, 255))
        self.wave_text = self.font.render(f"Wave: {self.wave}", 1, (255, 255, 255))
        m = self.game_timer // 60
        s = self.game_timer % 60
        self.time_text = self.font.render(f"Time: {m:02d}:{s:02d}", 1, (255, 255, 255))

    def update(self):
        if self.areas.room is not self.arena:  # Only spawn enemies in the arena
            #  if not len(self.npc_group.sprites()):
            #      self.spawn_wizard((600, 150))
            return

        if not self.game_started:

            self.start_game()

        elapsed = (pygame.time.get_ticks() - self.start_time) // 1000
        self.game_timer = self.countdown - elapsed  # Game time in seconds

        wave = elapsed // 60 + 1
        if wave != self.wave:
            # New Wave!
            print(f"New Wave!!!")
            self.wave = wave

        self.update_overlay()
        spawn_rate = 5
        if self.game_timer > 480:  # 8 minutes left
            spawn_rate = 5
        elif self.game_timer > 300:  # 5 minutes left
            spawn_rate = 10
        elif self.game_timer > 180:  # 3 minutes left
            spawn_rate = 15

        if self.game_timer % spawn_rate == 0:  # Spawn enemies ever 10 seconds
            if self.spawn_switch:
                self.spawn_switch = False
                for _ in range(self.wave):
                    spawn_point = self.spawn_points[randint(0, len(self.spawn_points) - 1)]
                    self.spawn_enemy(spawn_point)
                    print("SPAWN ENEMY")
        elif not self.spawn_switch:
            self.spawn_switch = True

    def render(self, surface):
        if self.areas.room is not self.arena:  # Only render overlay once we enter the arena
            return
        surface.blit(self.point_text, self.point_text.get_rect(topleft=self.OVERLAY_POINTS))
        surface.blit(self.health_text, self.point_text.get_rect(topleft=self.OVERLAY_HEALTH))
        surface.blit(self.wave_text, self.point_text.get_rect(topleft=self.OVERLAY_WAVE))
        surface.blit(self.time_text, self.point_text.get_rect(topleft=self.OVERLAY_TIME))
