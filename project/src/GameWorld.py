import os
import sys
import traceback

import pygame
from pygame.locals import K_DOWN, K_ESCAPE, K_LEFT, K_RIGHT, K_SPACE, K_UP, K_a, K_d, K_s, K_w, QUIT

from .AnimatedSprite import AnimatedSpriteGroup
from .GameState import GameState
from .InputComponent import InputComponent, LEFT_MOUSE, RIGHT_MOUSE  # NOQA
from .level.environment import Area, Arena, Room
from .Player.PlayerSprite import PlayerSprite
from .Utils import load_image


class World:

    # Fixed update time step
    MS_PER_UPDATE = 10

    GAME_TITLE = "Threat Level Midnight"

    # Frame rate
    FRAME_RATE = 0  # 0 == Uncapped TODO: Move to config file

    SCREEN_SIZE = (1280, 720)  # 16:9 aspect ratio TODO: Move to config file

    DEBUG = False  # TODO: Move to config file

    def __init__(self):
        # 1. Initialize game config
        self.init()

        # 2. Load game assets
        self.areas = Area()
        self.areas.add_room('start', Room((int(self.SCREEN_SIZE[0]*.96), int(self.SCREEN_SIZE[1]*.9)), self.areas))
        self.areas.add_room('arena', Arena(self.SCREEN_SIZE, self.areas))
        self.areas.set_room('start')
        center = self.SCREEN_SIZE[0] // 2, self.SCREEN_SIZE[1] // 2
        self.areas.room.display(self.screen, center)
        self.level_assets = pygame.sprite.Group()
        self.dead_message = load_image(os.path.join('project', 'res', 'assets', 'sprites', 'you_died.png'))
        self.win_message = load_image(os.path.join('project', 'res', 'assets', 'sprites', 'you_win.png'))

        # 2.1 Load Player Character
        self.character_sprites = AnimatedSpriteGroup(self.areas.room, debug=self.DEBUG)
        start_position = [576, 296]
        self.player = PlayerSprite(start_position, debug=self.DEBUG)
        self.player.set_triggers(self.areas.room.triggers)
        self.character_sprites.add(self.player)

        # 2.2 Load NPCs
        self.npc_group = AnimatedSpriteGroup(self.areas.room, debug=self.DEBUG)

        # 3. Load inital scene (Menu/Level 1/etc...)
        self.game_state = GameState(self.player, self.npc_group, self.areas, self.DEBUG)

    def init(self):
        # Fix for issue with delay in playing sound effects
        # https://stackoverflow.com/questions/18273722/pygame-sound-delay
        pygame.mixer.pre_init(44100, -16, 1, 1024)
        pygame.mixer.init()
        pygame.init()

        self.init_input_component()  # TODO: Add ability for players to remap their controllers via the UI

        self.screen = pygame.display.set_mode(self.SCREEN_SIZE)

        pygame.display.set_caption(self.GAME_TITLE)

        self.clock = pygame.time.Clock()

        # TODO Replace with proper menu scene
        self.background = pygame.Surface(self.screen.get_size()).convert()
        self.background.fill((0, 0, 0))
        self.screen.blit(self.background, (0, 0))

        pygame.display.flip()

    def init_input_component(self):
        self.input_component = InputComponent(debug=self.DEBUG)

        # UI Bindings
        self.input_component.bind_action('ui_cancel', QUIT)
        self.input_component.bind_action('ui_cancel', K_ESCAPE)
        self.input_component.set_callback('ui_cancel', self.on_quit_game)

        # Movement Bindings
        self.input_component.bind_action('walk_left', K_a)
        self.input_component.bind_action('walk_left', K_LEFT)
        self.input_component.bind_action('walk_down', K_s)
        self.input_component.bind_action('walk_down', K_DOWN)
        self.input_component.bind_action('walk_right', K_d)
        self.input_component.bind_action('walk_right', K_RIGHT)
        self.input_component.bind_action('walk_up', K_w)
        self.input_component.bind_action('walk_up', K_UP)
        self.input_component.bind_action('fire', LEFT_MOUSE)
        # self.input_component.bind_action('explode', RIGHT_MOUSE)
        self.input_component.bind_action('roll', K_SPACE)

    def process_input(self):
        self.input_component.process_events()
        self.character_sprites.handle_input(self.input_component)
        self.npc_group.handle_input(self.input_component)

    def update(self):
        # Check for win condition before updating to prevent player getting
        # cheated out of victory
        if self.game_state.player_has_won():
            self.player.god_mode = True

        # Handle collisions with level assets
        self.character_sprites.handle_collision(self.level_assets)
        self.npc_group.handle_collision(self.level_assets)
        self.character_sprites.handle_triggers()

        # Handle collisions with other in-game characters
        self.character_sprites.handle_collision(self.npc_group)
        self.npc_group.handle_collision(self.character_sprites)

        # Handle Projectile collisions
        self.player.projectiles.handle_collision(self.npc_group)

        # Update sprite groups and whatever else that's a
        # part of the game simulation.
        self.character_sprites.update()
        self.npc_group.update()

        # Update game state
        self.game_state.update()

    def render(self):
        # TODO: We can probably remove this once we've added scenes as
        # blitting will be incorporated within each scene object.
        self.screen.blit(self.background, (0, 0))

        center = self.SCREEN_SIZE[0] // 2, self.SCREEN_SIZE[1] // 2
        self.areas.room. display(self.screen, center)

        self.character_sprites.draw(self.screen)

        self.npc_group.draw(self.screen)

        # Render game state
        self.game_state.render(self.screen)

        if len(self.character_sprites) == 0:
            image = self.dead_message[0]
            rect = self.dead_message[1]
            rect.center = self.screen.get_rect().center
            self.screen.blit(image, rect)

        if self.game_state.player_has_won():
            image = self.win_message[0]
            rect = self.win_message[1]
            rect.center = self.screen.get_rect().center
            self.screen.blit(image, rect)

        pygame.display.flip()

    def game_loop(self):
        lag = 0
        while True:
            elapsed_time = self.clock.tick(self.FRAME_RATE)
            lag += elapsed_time

            self.process_input()

            # Update using a fixed time step. That way slow machines will run
            # multiple updates to catch up to realtime and fast machines
            # will only update on the fixed time step. This ensures a
            # consistent simulation regardless of system performance as
            # well as helps us get around the issues involving a variable
            # time step.
            while lag >= self.MS_PER_UPDATE:
                self.update()
                lag -= self.MS_PER_UPDATE

            self.render()

            pygame.display.set_caption(f"{self.GAME_TITLE} | FPS: {self.clock.get_fps():3.0f}")

    def run_game(self):
        try:
            self.game_loop()
        except Exception as msg:
            print(f"An Exception has occurred: {msg}")  # TODO replace with a core.log
            traceback.print_exc(file=sys.stdout)

    def on_quit_game(self, action, event):
        print("Quitting Game...")
        pygame.quit()
        sys.exit()
