import itertools
import os
import random

import pygame

from .coordinatemap import Coordinates
from ..Utils import load_config

"""
Texture loading inspired from:
https://stackoverflow.com/questions/27871389/texturing-drawn-shapes-python-pygame
"""


class MetaAsset(type):
    """
    Enforces user code will implement required methods.
    """
    CONFIG = load_config(os.path.join('project', 'src', 'level', '.config.json'))
    FOLDER = os.path.join(*CONFIG['path']['base'])

    def __new__(cls, name, bases, namespace, **kwds):
        path = cls.CONFIG['path'].get(name)
        if path:
            namespace['path'] = os.path.join(cls.FOLDER, path)

        required_methods = cls.CONFIG.get("required")
        for method in required_methods:
            if method not in namespace:
                raise TypeError(f'{name} must implement "{method}"')
        return super().__new__(cls, name, bases, namespace, **kwds)


class Asset(metaclass=MetaAsset):
    """
    Parent Descripter that handles asset manipulation
    """

    def __set_name__(self, owner, name):
        self.name = name

    def __set__(self, instance, dimensions):
        self.width, self.height = dimensions
        self.surface = pygame.Surface(dimensions, depth=32)
        print(self.surface.get_rect())
        self.mask = pygame.Surface(dimensions, depth=32)
        self.images = list(filter(self.find_assets, os.listdir(self.path)))
        self.texture = self.load_textures()
        tsize = self.texture_size
        self.columns, self.rows = self.width // tsize[0], self.height // tsize[1]
        self.coords = Coordinates(dimensions, self.texture_size)
        self.apply_texture()
        self.draw()
        instance.__dict__[self.name] = self.apply_alpha()

    def __get__(self, instance, owner):
        return instance.__dict__[self.name]

    def load_textures(self):
        """
        Converts all loaded image files into dict of image objects
        where file name is the key, object is the value
        """
        if len(self.images) > 1:
            return {name: self.load_asset(name) for name in self.images}
        if self.images:
            return self.load_asset(self.images[0])
        else:
            raise FileNotFoundError("Image(s) not found")

    def apply_texture(self):
        """
        Apply textures to surface and draw them to mask
        Asset subclasses with multiple images must define their own
        `apply_texture` method
        """
        if isinstance(self.texture, dict):
            raise ValueError(
                "Multiple images detected. "
                "Please define a custom `apply_texture` method"
            )
        else:
            self.tile_texture()

    def load_asset(self, file: str):
        """
        Return image object loaded from file path given
        params:
            file - file name
        """
        path = os.path.join(self.path, file)
        asset = pygame.image.load(path).convert_alpha()
        asset = pygame.transform.scale(asset, (32, 32))
        return asset

    def draw(self):
        """
        Draws texture to shape, default is rectangle
        """
        pygame.draw.rect(self.mask, 255, self.surface.get_rect(), 0)

    def find_assets(self):
        """
        Virtual method for asset discovery
        """
        pass

    def tile_texture(self, texture=None, coords=None):
        """
        Applys `texture` to `surface`
        """
        texture = texture or self.texture
        coords = coords or self.coords
        for coord in coords:
            self.surface.blit(texture, coord)

    def merge_textures(self, *textures):
        """
        Layers all given texture objects on top of each other
        textures: List[pygame.image]
        """
        textures = iter(textures)
        merged = next(textures).copy()
        for texture in textures:
            merged.blit(texture, (0, 0))
        return merged

    def apply_alpha(self):
        """
        Image should be  a 24 or 32bit image,
        mask should be an 8 bit image with the alpha
        channel to be applied
        """
        texture = self.surface.convert_alpha()
        target = pygame.surfarray.pixels_alpha(texture)
        target[:] = pygame.surfarray.array2d(self.mask)
        # surfarray objets usually lock the Surface.
        # it is a good idea to dispose of them explicitly
        # as soon as the work is done.
        del target
        return texture

    @property
    def texture_size(self):
        """
        Dimensions of the loaded texture(s)
        """
        if isinstance(self.texture, dict):
            texture = next(iter(self.texture.values()))
        else:
            texture = self.texture
        return texture.get_width(), texture.get_height()

    @property
    def size(self):
        """
        Dimensions of total surface
        """
        return self.width, self.height

    @property
    def max_x(self):
        """
        The maximum value on the x axis
        """
        return int(self.columns)

    @property
    def max_y(self):
        """
        The maximum value on the y axis
        """
        return int(self.rows) - 1


class Floor(Asset):

    def apply_texture(self):
        """
        Overloaded for multiple textures
        """
        textures = (
            (self.texture.get('floor_2.png'), self.get_random_locations(20)),
            (self.texture.get('floor_3.png'), self.get_random_locations(10)),
            (self.texture.get('floor_4.png'), self.get_random_locations(10)),
            (self.texture.get('floor_5.png'), self.get_random_locations(10)),
            (self.texture.get('floor_6.png'), self.get_random_locations(10)),
            (self.texture.get('floor_7.png'), self.get_random_locations(10)),
            (self.texture.get('floor_8.png'), self.get_random_locations(10)),
            (self.texture.get('floor_1.png'), self.coords)
        )
        for texture, coords in textures:
            self.tile_texture(texture, coords)

    def draw(self):
        """
        Draws texture to shape, default is rectangle
        """
        pygame.draw.rect(self.mask, 125, self.surface.get_rect(), 0)

    def get_random_locations(self, amount):
        """
        Generates a random location `amount` times.
        Good for randomizing floor times
        """
        def rand_coords():
            randy = random.randint(0, self.rows-1)
            randx = random.randint(0, self.columns-1)
            return randx, randy
        return itertools.chain(*(self.coords.get_positions(*rand_coords()) for _ in range(amount)))

    def find_assets(self, name):
        """
        For finding the asset files in the folder
        """
        return 'floor' in name


class Wall(Asset):

    def apply_texture(self):
        """
        Entry point for wall asset decision makin
        """
        textures = getattr(self, self.name)()
        for name, coord in textures:
            if coord:
                coords = self.coords.get_positions(*coord)
            else:
                coords = coord
            if isinstance(name, list):
                txts = (self.texture.get(n) for n in name)
                texture = self.merge_textures(*txts)
            else:
                texture = self.texture.get(name)
            self.tile_texture(texture, coords)

    def northw(self):
        cl = (0, 1, None, self.max_y+1)
        ctl = (0, 0)
        top = (1, 0, self.max_x+1)
        tr = (self.max_x, 0)
        cl_front = (1, self.max_y)
        corner_right = (self.max_x, 0, None, self.max_y+1)

        return (
            ('wall_corner_top_left.png', ctl),
            ('wall_corner_left.png', cl),
            ('wall_top_right.png', tr),
            ('wall_corner_front_left.png', cl_front),
            (['wall_mid.png', 'wall_side_mid_right.png'], corner_right),
            ('wall_top_mid.png', top),
            ('wall_mid.png', None)
        )

    def southw(self):
        top = (0, 0, self.max_x+1)
        corner_left = (0, 0)
        corner_right = (self.max_x, 0)

        return (
            (['wall_corner_front_left.png', 'wall_corner_bottom_left.png'], corner_left),
            (['wall_corner_front_right.png', 'wall_corner_bottom_left.png'], corner_right),
            (['wall_mid.png', 'wall_top_mid.png'], top)
        )

    def westw(self):
        top = (0, 0, None, self.max_y+1)

        return (
            (['wall_left.png', 'wall_side_mid_right.png'], top),
            ('wall_left.png', None),
        )

    def eastw(self):
        top = (0, 0, None, self.max_y+1)

        return (
            (['wall_right.png', 'wall_side_mid_left.png', 'wall_corner_left.png'], top),
            ('wall_right.png', None),
        )

    def find_assets(self, name):
        return 'wall' in name or name == 'floor_1.png'


class Door(Asset):

    def apply_texture(self):
        self.closed = self.texture.get('doors_leaf_closed.png')
        self.opened = self.texture.get('doors_leaf_open.png')
        self.close_door()

    def open_door(self):
        self.tile_texture(self.opened)

    def close_door(self):
        self.tile_texture(self.closed)

    def find_assets(self, name):
        return 'door' in name
