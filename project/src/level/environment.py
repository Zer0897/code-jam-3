import pygame

from .assets import Door, Floor, Wall


class Environment:

    def __init__(self, size, areas):
        self.size = size
        self.areas = areas
        self.setup()

    def setup(self):
        pass

    def center(self, x, y):
        return self.centerx(x), self.centery(y)

    def centerx(self, x):
        return (self.x - x) // 2

    def centery(self, y):
        return (self.y - y) // 2

    def shrink(self, coords, prcnt):
        x, y = coords
        return coords[0] * prcnt, coords[1] * prcnt

    def partion(self, percent: float, divs: int) -> tuple:
        return self.partionx(percent) // divs, self.partiony(percent) // divs

    def partionx(self, percent, divs=1):
        return self.partion_point(percent, self.x) // divs

    def partiony(self, percent, divs=1):
        return self.partion_point(percent, self.y) // divs

    def partion_point(self, prcnt, point):
        return (point * prcnt)

    def size_diff(self, l1, l2, divs=1):
        return (l1 - l2) // divs

    @property
    def size(self):
        return self.x, self.y

    @size.setter
    def size(self, size):
        self.x, self.y = size


class Room(Environment):
    floor = Floor()
    northw = Wall()
    southw = Wall()
    westw = Wall()
    eastw = Wall()
    door = Door()

    def setup(self):
        self.floor = self.floorsize
        self.northw = self.ns_wallsize
        self.southw = self.ns_wallsize
        self.westw = self.ew_wallsize
        self.eastw = self.ew_wallsize
        self.door = 32, 32

        self.triggers = (
            (pygame.Rect(
                (self.center_door[0]+70, self.center_door[1]+22),
                self.door.get_rect().size), lambda: self.areas.set_room('arena'))
        )

    def display(self, screen, location, **kwargs):
        self.location = location
        screen.blit(self.floor, self.center, **kwargs)
        screen.blit(self.northw, self.top, **kwargs)
        self.northw.blit(self.door, self.center_door, **kwargs)
        screen.blit(self.southw, self.bottom, **kwargs)
        screen.blit(self.westw, self.left, **kwargs)
        screen.blit(self.eastw, self.right, **kwargs)

    def align_center(self, location):
        (lx, ly), (fx, fy) = location, self.floorsize
        return lx - (fx // 2), ly - fy // 2

    def align_top(self, location):
        (cx, cy), fx, (wx, wy) = self.align_center(location), self.floorsize[0], self.ns_wallsize
        return cx - self.size_diff(wx, fx, 2), cy - wy

    def align_bottom(self, location):
        (cx, cy), (fx, fy), wx = self.align_center(location), self.floorsize, self.ns_wallsize[0]
        return cx - self.size_diff(wx, fx, 2), cy + fy

    def align_left(self, location):
        cx, cy = self.align_center(location)
        return cx, cy

    def align_right(self, location):
        (cx, cy), fx, wx = self.align_center(location), self.floorsize[0], self.ew_wallsize[0]
        return (cx + fx) - wx/2 - 4, cy

    @property
    def center(self):
        return self.align_center(self.location)

    @property
    def top(self):
        return self.align_top(self.location)

    @property
    def bottom(self):
        return self.align_bottom(self.location)

    @property
    def left(self):
        return self.align_left(self.location)

    @property
    def right(self):
        return self.align_right(self.location)

    @property
    def center_door(self):
        return self.ns_wallsize[0]//2, 35

    @property
    def floorsize(self):
        return self.shrink(self.size, 0.897)

    @property
    def ns_wallsize(self):
        (fx, fy), sy = self.floorsize, self.size[1]
        return fx, self.size_diff(sy, fy)

    @property
    def ew_wallsize(self):
        ((fx, fy)), (sx, sy) = self.floorsize, self.size
        return self.size_diff(sx, fx, 7), sy - self.size_diff(sy, fy)


class Arena(Room):

    def setup(self):
        self.floor = self.size
        self.southw = self.ns_wallsize
        self.westw = self.ew_wallsize[0], self.size[1]
        self.eastw = self.ew_wallsize[0], self.size[1]
        self.door = 32, 32

    def display(self, screen, location, **kwargs):
        self.location = location
        screen.blit(self.floor, (0, 0), **kwargs)
        screen.blit(self.westw, self.left, **kwargs)
        screen.blit(self.eastw, self.right, **kwargs)
        screen.blit(self.southw, self.bottom, **kwargs)
        self.southw.blit(self.door, self.center_door)

    @property
    def center_door(self):
        return self.ns_wallsize[0]//2, 500

    def align_left(self, location):
        cx, cy = self.align_center(location)
        return cx, 0

    def align_right(self, location):
        cx, fx, wx = self.align_center(location)[0], self.floorsize[0], self.ew_wallsize[0]
        return (cx + fx) - wx/2 - 18, 0


class Area(dict):

    room = None

    def add_room(self, name, room):
        self[name] = room

    def set_room(self, name):
        self.room = self.get(name)
