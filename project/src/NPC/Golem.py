import os
from random import randint

import pygame
import pyganim

from ..AnimatedSprite import AnimatedSprite
from ..Utils import load_sound


class GolemSprite(AnimatedSprite):

    # Facing
    UP = 'up'
    RIGHT = 'right'
    DOWN = 'down'
    LEFT = 'left'

    DAMAGE = 5
    HEALTH = 30

    def __init__(self, player, position=None, debug=False, point_callback=None):
        AnimatedSprite.__init__(self, position, None, None, debug)
        self.init()
        self.current_state = None
        self.facing = self.RIGHT
        self.damage = self.DAMAGE
        self.point_callback = point_callback

        self.player = player
        self.screen = pygame.Rect(0, 0, 0, 0)

        position = position if position else (0, 0)
        self.rect = pygame.Rect(position, (0, 0))

        self.init_animations()
        self.init_state()

        self.speed = (0, 0)

    def init(self):
        self.health = self.HEALTH
        self.init_sound()

    def init_sound(self):
        self.sound = {}
        self.sound['damage'] = [
            load_sound(os.path.join('NPC', 'big_demon_damage_0.ogg')),
            load_sound(os.path.join('NPC', 'big_demon_damage_1.ogg')),
            load_sound(os.path.join('NPC', 'big_demon_damage_2.ogg'))
        ]

    def take_damage(self, damage):
        self.health -= damage
        audio_int = randint(0, 2)
        self.sound['damage'][audio_int].play()
        if self.health > 0:
            print(f"Golem took {damage} damage | Health Remaining: {self.health}")
        else:
            print(f"A Golem was killed...")
            if self.point_callback is not None:
                self.point_callback()
            self.kill()

    def sees_player(self):
        # {'size': (80, 114), 'offset': (25, 0)}
        vert_rect = pygame.Rect(self.rect.left + 25, self.screen.top, 80, self.screen.height)
        hori_rect = pygame.Rect((self.screen.left, self.rect.top, self.screen.width, 114))
        if vert_rect.colliderect(self.player.rect):
            if self.player.rect.centery > self.rect.centery and self.facing == self.DOWN:  # Player is above
                return True
            elif self.player.rect.centery < self.rect.centery and self.facing == self.UP:
                return True
        elif hori_rect.colliderect(self.player.rect):
            if self.player.rect.centerx > self.rect.centerx and self.facing == self.RIGHT:
                return True
            elif self.player.rect.centerx < self.rect.centerx and self.facing == self.LEFT:
                return True

        return False

    def update(self):
        if self.facing == self.RIGHT:
            self.rect.centerx += self.speed[0]
        elif self.facing == self.DOWN:
            self.rect.centery += self.speed[1]
        elif self.facing == self.LEFT:
            self.rect.centerx -= self.speed[0]
        elif self.facing == self.UP:
            self.rect.centery -= self.speed[1]
        self.screen_bump()

    def screen_bump(self):
        if self.rect.right > self.screen.right:
            self.rect.right -= 5
        elif self.rect.left < self.screen.left:
            self.rect.left += 5
        elif self.rect.top < self.screen.top:
            self.rect.top += 5
        elif self.rect.bottom > self.screen.bottom:
            self.rect.bottom -= 5

    def handle_input(self, input_component):
        state = self.state[-1].handle_input(input_component)
        if state and state is not self.state[-1]:
            self.state.append(state)
            state.enter(self)
            if self.debug:
                print(f"Golem State Stack: {self.state}")
        elif not state:
            self.pop_state()

    def deals_damage(self):
        return self.damage > 0

    def get_hurtbox(self):
        offset = self.hurtboxes[self.current_state][self.facing]['offset']
        size = self.hurtboxes[self.current_state][self.facing]['size']
        return pygame.Rect((self.rect.left + offset[0], self.rect.top + offset[1]), size)

    def init_animations(self):
        self.animations = {}
        self.hurtboxes = {}
        self.load_idle_animation()
        self.load_walking_animation()
        self.load_charging_animation()

    def set_animation(self, state, name):
        if state in self.animations and name in self.animations[state]:
            self.rect.width = self.animations[state][name].getRect().width
            self.rect.height = self.animations[state][name].getRect().height
            self.current_animation = self.animations[state][name]
            self.animations[state][name].play()

    def load_idle_animation(self):
        path = os.path.join(self.ASSET_DIR, 'npc', 'golem', 'idle')
        self.animations['idle'] = {}
        self.hurtboxes['idle'] = {}

        # Idle Up
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Up.png'), rows=1, cols=1, rects=[])
        frames = list(zip(images, [100]))
        self.animations['idle']['up'] = pyganim.PygAnimation(frames)
        self.hurtboxes['idle']['up'] = {'size': (80, 114), 'offset': (25, 0)}

        # Idle Right
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Right.png'), rows=1, cols=1, rects=[])
        frames = list(zip(images, [100]))
        self.animations['idle']['right'] = pyganim.PygAnimation(frames)
        self.hurtboxes['idle']['right'] = {'size': (62, 107), 'offset': (14, 13)}

        # Idle Down
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Down.png'), rows=1, cols=1, rects=[])
        frames = list(zip(images, [100]))
        self.animations['idle']['down'] = pyganim.PygAnimation(frames)
        self.hurtboxes['idle']['down'] = {'size': (80, 114), 'offset': (25, 0)}

        # Idle Left
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Left.png'), rows=1, cols=1, rects=[])
        frames = list(zip(images, [100]))
        self.animations['idle']['left'] = pyganim.PygAnimation(frames)
        self.hurtboxes['idle']['left'] = {'size': (62, 107), 'offset': (52, 13)}

    def load_walking_animation(self):
        path = os.path.join(self.ASSET_DIR, 'npc', 'golem', 'walk')
        self.animations['walk'] = {}
        self.hurtboxes['walk'] = {}

        # Walk Up
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Walk_Up.png'), rows=1, cols=7, rects=[])
        frames = list(zip(images, [100, 100, 100, 100]))
        self.animations['walk']['up'] = pyganim.PygAnimation(frames)
        self.hurtboxes['walk']['up'] = {'size': (80, 114), 'offset': (25, 0)}

        # Walk Right
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Walk_Right.png'), rows=1, cols=7, rects=[])
        frames = list(zip(images, [100, 100, 100, 100]))
        self.animations['walk']['right'] = pyganim.PygAnimation(frames)
        self.hurtboxes['walk']['right'] = {'size': (62, 107), 'offset': (14, 13)}

        # Walk Down
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Walk_Down.png'), rows=1, cols=7, rects=[])
        frames = list(zip(images, [100, 100, 100, 100]))
        self.animations['walk']['down'] = pyganim.PygAnimation(frames)
        self.hurtboxes['walk']['down'] = {'size': (80, 114), 'offset': (25, 0)}

        # Walk Left
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Walk_Left.png'), rows=1, cols=7, rects=[])
        frames = list(zip(images, [100, 100, 100, 100]))
        self.animations['walk']['left'] = pyganim.PygAnimation(frames)
        self.hurtboxes['walk']['left'] = {'size': (62, 107), 'offset': (52, 13)}

    def load_charging_animation(self):
        path = os.path.join(self.ASSET_DIR, 'npc', 'golem', 'walk')
        self.animations['charge'] = {}
        self.hurtboxes['charge'] = {'size': (), 'offset': ()}

        # Charge Up
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Walk_Up.png'), rows=1, cols=7, rects=[])
        frames = list(zip(images, [50, 50, 50, 50]))
        self.animations['charge']['up'] = pyganim.PygAnimation(frames)
        self.hurtboxes['charge']['up'] = {'size': (80, 114), 'offset': (25, 0)}

        # Charge Right
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Walk_Right.png'), rows=1, cols=7, rects=[])
        frames = list(zip(images, [50, 50, 50, 50]))
        self.animations['charge']['right'] = pyganim.PygAnimation(frames)
        self.hurtboxes['charge']['right'] = {'size': (62, 107), 'offset': (14, 13)}

        # Charge Down
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Walk_Down.png'), rows=1, cols=7, rects=[])
        frames = list(zip(images, [50, 50, 50, 50]))
        self.animations['charge']['down'] = pyganim.PygAnimation(frames)
        self.hurtboxes['charge']['down'] = {'size': (80, 114), 'offset': (25, 0)}

        # Charge Left
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Walk_Left.png'), rows=1, cols=7, rects=[])
        frames = list(zip(images, [50, 50, 50, 50]))
        self.animations['charge']['left'] = pyganim.PygAnimation(frames)
        self.hurtboxes['charge']['left'] = {'size': (62, 107), 'offset': (52, 13)}

    def draw(self, surface):
        AnimatedSprite.draw(self, surface)
        self.screen = surface.get_rect()

    def init_state(self):
        state = Idle()
        self.state.append(state)
        state.enter(self)


class State:
    def __init__(self):
        self.start_time = pygame.time.get_ticks()

    def get_facing(self):
        val = randint(0, 3)
        if val == 0:
            return 'right'
        elif val == 1:
            return 'down'
        elif val == 2:
            return 'left'
        elif val == 3:
            return 'up'
        return 'down'

    def handle_input(self, input_component):
        return self

    def enter(self, player):
        pass

    def exit(self, player):
        pass


class Idle(State):
    def __init__(self):
        State.__init__(self)
        self.state_duration = 2000
        self.player = None

    def handle_input(self, input_component):
        if self.player and self.player.sees_player():
            return Charge()  # Charge the MOFO...
        _time = pygame.time.get_ticks()
        if _time - self.start_time > self.state_duration:  # Inject new input
            return Walk()
        return self

    def enter(self, player):
        self.player = player
        player.facing = self.get_facing()
        player.set_animation('idle', player.facing)
        player.current_state = 'idle'
        player.speed = (0, 0)


class Walk(State):
    def __init__(self):
        State.__init__(self)
        self.state_duration = 2000
        self.player = None

    def handle_input(self, input_component):
        if self.player and self.player.sees_player():
            return Charge()  # Charge the MOFO...
        _time = pygame.time.get_ticks()
        if _time - self.start_time > self.state_duration:  # Inject new input
            return None  # Pop ourselves off the stack so we can go back to Idle
        return self  # Otherwise, just keep walking...

    def enter(self, player):
        self.player = player
        player.set_animation('walk', player.facing)
        player.current_state = 'walk'
        player.speed = (2, 2)


class Charge(State):
    def __init__(self):
        State.__init__(self)
        self.state_duration = 1000
        self.player = None

    def handle_input(self, input_component):
        _time = pygame.time.get_ticks()
        if _time - self.start_time >= self.state_duration:  # Inject new input
            return None
        return self

    def enter(self, player):
        self.player = player
        player.set_animation('charge', player.facing)
        player.current_state = 'charge'
        player.speed = (4, 4)
