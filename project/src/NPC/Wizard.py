import os
from random import randint  # NOQA

import pygame
import pyganim

from ..AnimatedSprite import AnimatedSprite


class WizardSprite(AnimatedSprite):

    # Facing
    UP = 'up'
    RIGHT = 'right'
    DOWN = 'down'
    LEFT = 'left'

    DAMAGE = 0
    HEALTH = 1

    def __init__(self, player, position=None, debug=False):
        AnimatedSprite.__init__(self, position, None, None, debug)
        self.init()
        self.current_state = None
        self.facing = self.RIGHT
        self.damage = self.DAMAGE
        # self.state = []
        self.player = player
        self.screen = pygame.Rect(0, 0, 0, 0)

        position = position if position else (0, 0)
        self.rect = pygame.Rect(position, (0, 0))

        self.init_animations()
        self.init_state()

        self.speed = (0, 0)

    def init(self):
        self.health = self.HEALTH

    def deals_damage(self):
        return self.damage > 0

    def take_damage(self, *args):
        pass

    def get_hurtbox(self):
        offset = self.hurtboxes[self.current_state]['offset']
        size = self.hurtboxes[self.current_state]['size']
        return pygame.Rect((self.rect.left + offset[0], self.rect.top + offset[1]), size)

    def init_animations(self):
        self.animations = {}
        self.hurtboxes = {}
        self.load_idle_animation()

    def set_animation(self, state):
        self.rect.width = self.animations[state].getRect().width
        self.rect.height = self.animations[state].getRect().height
        self.current_animation = self.animations[state]
        self.animations[state].play()

    def load_idle_animation(self):
        image = os.path.join(self.ASSET_DIR, 'npc', 'wizard idle.png')
        self.animations['idle'] = {}
        self.hurtboxes['idle'] = {}

        images = pyganim.getImagesFromSpriteSheet(image, rows=1, width=50, height=80, cols=8, rects=[])
        images = [image.convert_alpha() for image in images]
        frames = list(zip(images, [200]*8))
        self.animations['idle'] = pyganim.PygAnimation(frames)
        self.hurtboxes['idle'] = {'size': (80, 114), 'offset': (25, 0)}

    def draw(self, surface):
        AnimatedSprite.draw(self, surface)
        self.screen = surface.get_rect()

    def init_state(self):
        state = Idle()
        self.state.append(state)
        state.enter(self)


class State:
    def __init__(self):
        self.start_time = pygame.time.get_ticks()

    def handle_input(self, input_component):
        return self

    def enter(self, player):
        pass

    def exit(self, player):
        pass


class Idle(State):
    def __init__(self):
        State.__init__(self)
        self.state_duration = 2000
        self.player = None

    def enter(self, player):
        self.player = player
        player.set_animation('idle')
        player.current_state = 'idle'
        player.speed = (0, 0)
