import pygame
from pygame.locals import KEYDOWN, KEYUP, MOUSEBUTTONDOWN, MOUSEBUTTONUP

LEFT_MOUSE = "MOUSE_1"
MIDDLE_MOUSE = "MOUSE_2"
RIGHT_MOUSE = "MOUSE_3"


class InputComponent:
    """InputComponent class for grouping pygame events under a unique action event,
    as well as assigning callback methods to said action events. It is also used
    to determine which action events are being triggered on the current frame."""

    def __init__(self, debug=False):
        self.input_mapping = {}
        self.key_map = {}
        self.debug = debug

    def bind_action(self, action_string, key_event, callback_fn=None):
        if action_string in self.input_mapping:
            self.input_mapping[action_string]['key_events'].add(key_event)
        else:
            self.input_mapping[action_string] = {
                'key_events': set([key_event]),
                'callbacks': set(),
                'is_pressed': False
            }

        # Add key_event to key map
        if key_event in self.key_map:
            self.key_map[key_event].add(action_string)
        else:
            self.key_map[key_event] = set([action_string])

        if self.debug:
            print(f"Binding {key_event} to Action {action_string}")

        self.set_callback(action_string, callback_fn)

    def set_callback(self, action_string, callback_fn):
        if callback_fn and action_string in self.input_mapping:
            self.input_mapping[action_string]['callbacks'].add(callback_fn)

            if self.debug:
                print(f"Binding Action {action_string} to Callback {callback_fn}")

    # TODO: Make this asynchronous so bad callback functions wont affect framerate.
    def process_callback(self, action_string, events):
        if action_string in self.input_mapping:
            for fn in self.input_mapping[action_string]['callbacks']:
                if self.debug:
                    print(f"Processing callback for: {events}")
                fn(action_string, events)

    def is_action_pressed(self, action):
        if action not in self.input_mapping:
            return False

        return self.input_mapping[action]['is_pressed']

    def process_events(self):
        # Order is important here, pygame.event.get() must come
        # before pygame.mouse.get_pressed()
        # https://www.pygame.org/docs/ref/mouse.html#pygame.mouse.get_pressed
        events = pygame.event.get()
        keys_pressed = pygame.key.get_pressed()
        mouse_pressed = pygame.mouse.get_pressed()

        # 1. Determine if action is pressed
        for action_event in self.input_mapping:
            _break = False
            # 1.1 Keyboard actions pressed?
            for key_event in self.input_mapping[action_event]['key_events']:
                if isinstance(key_event, int):
                    try:
                        if keys_pressed[key_event]:
                            self.input_mapping[action_event]['is_pressed'] = True
                            _break = True
                            break
                    except Exception:  # NOQA
                        pass
                # 1.2 Mouse actions pressed?
                elif key_event == LEFT_MOUSE and mouse_pressed[0]:
                    self.input_mapping[action_event]['is_pressed'] = True
                    _break = True
                    break
                elif key_event == MIDDLE_MOUSE and mouse_pressed[1]:
                    self.input_mapping[action_event]['is_pressed'] = True
                    _break = True
                    break
                elif key_event == RIGHT_MOUSE and mouse_pressed[2]:
                    self.input_mapping[action_event]['is_pressed'] = True
                    _break = True
                    break
            if not _break:
                self.input_mapping[action_event]['is_pressed'] = False

        # 2 Process callbacks -- Callback functions are executed only
        # on the initial key down and key up events. They will NOT
        # continuously fire if a key is being held down.
        callback_events = {}
        for event in events:
            # 2.1 Determine which mouse button was pressed
            if event.type == MOUSEBUTTONDOWN or event.type == MOUSEBUTTONUP:
                key = f"MOUSE_{event.button}"
            # 2.2 Determine which keyboard key was pressed
            elif event.type == KEYDOWN or event.type == KEYUP:
                key = event.key
            else:
                key = event.type

            if key in self.key_map:
                # 2.3 Aggregate all events by action_string
                # for the current frame.
                action_strings = self.key_map[key]
                for action_string in action_strings:
                    if action_string in callback_events:
                        callback_events[action_string].append(event)
                    else:
                        callback_events[action_string] = [event]
        # 2.4 Execute callback functions, passing them the
        # action string and associated events.
        for callback in callback_events:
            if self.debug:
                print(f"Actions triggered: {callback}")
            self.process_callback(callback, callback_events[callback])
