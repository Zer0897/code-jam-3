import os
from random import randint

import pygame
import pyganim

from .Projectile import ProjectileGroup
from ..AnimatedSprite import AnimatedSprite
from ..Utils import load_sound


class PlayerSprite(AnimatedSprite):

    # Facing
    UP = 'up'
    RIGHT = 'right'
    DOWN = 'down'
    LEFT = 'left'

    # Character Config
    DEFAULT_WALK_SPEED = 2
    DEFAULT_ROLL_SPEED = 4
    DEFAULT_HEALTH = 100

    BOUNCE = 5
    DAMAGE_PUSHBACK = 25

    def __init__(self, position=None, debug=False):
        AnimatedSprite.__init__(self, position, debug)
        # Setup Character stats
        self.init()

        self.input_configured = False
        self.current_state = None
        self.facing = self.DOWN  # Direction PC is facing
        self.pop_when_complete = False
        self.god_mode = False
        self.debug = debug

        # Damage Fields
        self.damage_timer = 2000
        self.taking_damage = False
        self.damage_start = 0
        self.damage_flip = True

        # Set Rect
        position = position if position else (0, 0)
        self.rect = pygame.Rect(position, (0, 0))

        self.init_animations()
        self.init_state()

        self.speed = (0, 0)
        self.walk_speed = self.DEFAULT_WALK_SPEED
        self.roll_speed = self.DEFAULT_ROLL_SPEED

        self.projectiles = ProjectileGroup(debug=True)
        self.triggers = list()

    def init(self):
        self.health = self.DEFAULT_HEALTH
        self.init_sound()

    def init_sound(self):
        self.sound = {}
        self.sound['damage'] = [
            load_sound(os.path.join('Player', 'player_damage_0.ogg')),
            load_sound(os.path.join('Player', 'player_damage_1.ogg')),
            load_sound(os.path.join('Player', 'player_damage_2.ogg')),
            load_sound(os.path.join('Player', 'player_damage_3.ogg')),
            load_sound(os.path.join('Player', 'player_damage_4.ogg')),
            load_sound(os.path.join('Player', 'player_damage_5.ogg')),
        ]

    def update(self):
        self.rect.x += self.speed[0]
        self.rect.y += self.speed[1]

        self.projectiles.update()

    def draw(self, surface):
        AnimatedSprite.draw(self, surface)
        if self.taking_damage and (pygame.time.get_ticks() - self.damage_start) < self.damage_timer:
            offsets = (40, 30)
            size = (45, 80)
            if self.damage_flip:
                surface.fill(pygame.Color(86, 18, 18), pygame.Rect((self.rect.left + offsets[0],
                                                                    self.rect.top + offsets[1]), size))
            self.damage_flip = not self.damage_flip
        else:
            self.taking_damage = False
        self.projectiles.draw(surface)

    def set_animation(self, state, name):
        if state in self.animations and name in self.animations[state]:
            self.rect.width = self.animations[state][name].getRect().width
            self.rect.height = self.animations[state][name].getRect().height
            self.current_animation = self.animations[state][name]
            self.animations[state][name].play()

    def fire(self):
        pos = (0, 0)
        if self.facing == self.UP:
            pos = self.rect.midtop
        if self.facing == self.RIGHT:
            pos = self.rect.midright
        if self.facing == self.DOWN:
            pos = self.rect.midbottom
        if self.facing == self.LEFT:
            pos = self.rect.midleft

        self.projectiles.spawn_projectile(self.facing, pos)

    def init_animations(self):
        self.load_idle_animations()
        self.load_walking_animations()
        self.load_rolling_animations()
        self.load_firing_animations()

    def load_idle_animations(self):
        path = os.path.join(self.ASSET_DIR, 'player', 'idle')
        self.animations['idle'] = {}

        # Idle Up
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Up.png'), rows=1, cols=1, rects=[])
        frames = list(zip(images, [100]))
        self.animations['idle']['up'] = pyganim.PygAnimation(frames)

        # Idle Right
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Right.png'), rows=1, cols=1, rects=[])
        frames = list(zip(images, [100]))
        self.animations['idle']['right'] = pyganim.PygAnimation(frames)

        # Idle Down
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Down.png'), rows=1, cols=1, rects=[])
        frames = list(zip(images, [100]))
        self.animations['idle']['down'] = pyganim.PygAnimation(frames)

        # Idle Left
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Left.png'), rows=1, cols=1, rects=[])
        frames = list(zip(images, [100]))
        self.animations['idle']['left'] = pyganim.PygAnimation(frames)

    def load_walking_animations(self):
        path = os.path.join(self.ASSET_DIR, 'player', 'walk')
        self.animations['walk'] = {}

        # Walk Up
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Walk_Up.png'), rows=1, cols=4, rects=[])
        frames = list(zip(images, [100, 100, 100, 100]))
        self.animations['walk']['up'] = pyganim.PygAnimation(frames)

        # Walk Right
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Walk_Right.png'), rows=1, cols=4, rects=[])
        frames = list(zip(images, [100, 100, 100, 100]))
        self.animations['walk']['right'] = pyganim.PygAnimation(frames)

        # Walk Down
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Walk_Down.png'), rows=1, cols=4, rects=[])
        frames = list(zip(images, [100, 100, 100, 100]))
        self.animations['walk']['down'] = pyganim.PygAnimation(frames)

        # Walk Left
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Walk_Left.png'), rows=1, cols=4, rects=[])
        frames = list(zip(images, [100, 100, 100, 100]))
        self.animations['walk']['left'] = pyganim.PygAnimation(frames)

    def load_rolling_animations(self):
        path = os.path.join(self.ASSET_DIR, 'player', 'roll')
        self.animations['roll'] = {}

        # Walk Up
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Roll_Up.png'), rows=1, cols=4, rects=[])
        frames = list(zip(images, [100, 100, 100, 100]))
        self.animations['roll']['up'] = pyganim.PygAnimation(frames, loop=False)

        # Walk Right
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Roll_Right.png'), rows=1, cols=4, rects=[])
        frames = list(zip(images, [100, 100, 100, 100]))
        self.animations['roll']['right'] = pyganim.PygAnimation(frames, loop=False)

        # Walk Down
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Roll_Down.png'), rows=1, cols=4, rects=[])
        frames = list(zip(images, [100, 100, 100, 100]))
        self.animations['roll']['down'] = pyganim.PygAnimation(frames, loop=False)

        # Walk Left
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Roll_Left.png'), rows=1, cols=4, rects=[])
        frames = list(zip(images, [100, 100, 100, 100]))
        self.animations['roll']['left'] = pyganim.PygAnimation(frames, loop=False)

    def load_firing_animations(self):
        path = os.path.join(self.ASSET_DIR, 'player', 'fire')
        self.animations['fire'] = {}

        # Walk Up
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Fire_Up.png'), rows=1, cols=5, rects=[])
        frames = list(zip(images, [50] * 4))
        self.animations['fire']['up'] = pyganim.PygAnimation(frames, loop=False)

        # Walk Right
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Fire_Right.png'), rows=1, cols=5, rects=[])
        frames = list(zip(images, [50] * 4))
        self.animations['fire']['right'] = pyganim.PygAnimation(frames, loop=False)

        # Walk Down
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Fire_Down.png'), rows=1, cols=5, rects=[])
        frames = list(zip(images, [50] * 4))
        self.animations['fire']['down'] = pyganim.PygAnimation(frames, loop=False)

        # Walk Left
        images = pyganim.getImagesFromSpriteSheet(os.path.join(path, 'Fire_Left.png'), rows=1, cols=5, rects=[])
        frames = list(zip(images, [50] * 4))
        self.animations['fire']['left'] = pyganim.PygAnimation(frames, loop=False)

    def init_state(self):
        state = Idle()
        self.state.append(state)
        state.enter(self)

    def get_hurtbox(self):
        offsets = (45, 30)
        size = (45, 80)
        return pygame.Rect(self.rect.left + offsets[0], self.rect.top + offsets[1], size[0], size[1])

    def handle_input(self, input_component):
        state = self.state[-1].handle_input(input_component)
        if state and state is not self.state[-1]:
            self.state.append(state)
            state.enter(self)
            if self.debug:
                print(f"Player State Stack: {self.state}")
        elif not state:
            self.pop_state()

        self.projectiles.handle_input(input_component)

    def process_collision(self, sprite):
        if self.get_hurtbox().colliderect(sprite.get_hurtbox()):
            self.speed = (0, 0)

            # Bounce away
            if self.facing == self.RIGHT:
                self.rect.x -= self.BOUNCE
            elif self.facing == self.LEFT:
                self.rect.x += self.BOUNCE
            elif self.facing == self.UP:
                self.rect.y += self.BOUNCE
            elif self.facing == self.DOWN:
                self.rect.y -= self.BOUNCE

            print(f"DAMAGE: {sprite.damage}")
            print(f"Deals damage: {sprite.deals_damage}")
            if not self.god_mode and not self.taking_damage and sprite.deals_damage():
                sound_int = randint(0, 5)
                self.sound['damage'][sound_int].play()

                self.health -= sprite.damage

                if self.health > 0:
                    self.damage_start = pygame.time.get_ticks()
                    self.taking_damage = True
                    print(f"Took {sprite.damage} | {self.health} remaining...")
                else:
                    # Trigger death...
                    print(f"YOU DIED")
                    self.kill()

    def handle_collision(self, collisions):
        # Handle collisions passed to us
        if collisions:
            for obj in collisions:
                self.process_collision(obj)

    def handle_triggers(self):
        clocation = self.get_hurtbox()
        for trigger, action in self.triggers:
            if clocation.colliderect(trigger):
                action()

    def set_triggers(self, *triggers: tuple):
        for trigger in triggers:
            self.triggers.append(trigger)


class State:
    def __init__(self):
        self.start_time = pygame.time.get_ticks()

    def handle_input(self, input_component):
        return self

    def enter(self, player):
        pass

    def exit(self, player):
        pass


class Idle(State):
    def handle_input(self, input_component):
        if input_component.is_action_pressed('walk_left'):
            return WalkLeft()
        if input_component.is_action_pressed('walk_right'):
            return WalkRight()
        if input_component.is_action_pressed('walk_up'):
            return WalkUp()
        if input_component.is_action_pressed('walk_down'):
            return WalkDown()
        if input_component.is_action_pressed('fire'):
            return Fire()
        if input_component.is_action_pressed('roll'):
            return Roll()
        return self

    def enter(self, player):
        player.set_animation('idle', player.facing)
        player.current_state = 'idle'
        player.speed = (0, 0)


class Fire(State):
    def __init__(self):
        State.__init__(self)

    def enter(self, player):
        player.speed = (0, 0)
        player.current_state = 'fire'
        player.set_animation(player.current_state, player.facing)
        player.fire()


class WalkRight(State):
    def __init__(self):
        State.__init__(self)

    def handle_input(self, input_component):
        if pygame.time.get_ticks() - self.start_time > 64:
            if not input_component.is_action_pressed('walk_right'):
                return None
            if input_component.is_action_pressed('roll'):
                return Roll()
            if input_component.is_action_pressed('fire'):
                return Fire()
            return self
        return self

    def enter(self, player):
        player.facing = player.RIGHT
        player.current_state = 'walk'
        player.set_animation(player.current_state, player.facing)
        player.speed = (player.walk_speed, 0)


class WalkLeft(State):
    def __init__(self):
        State.__init__(self)

    def handle_input(self, input_component):
        if pygame.time.get_ticks() - self.start_time > 64:
            if not input_component.is_action_pressed('walk_left'):
                return None
            if input_component.is_action_pressed('roll'):
                return Roll()
            if input_component.is_action_pressed('fire'):
                return Fire()
            return self
        return self

    def enter(self, player):
        player.facing = player.LEFT
        player.current_state = 'walk'
        player.set_animation(player.current_state, player.facing)
        player.speed = (-player.walk_speed, 0)


class WalkUp(State):
    def __init__(self):
        State.__init__(self)

    def handle_input(self, input_component):
        if pygame.time.get_ticks() - self.start_time > 64:
            if not input_component.is_action_pressed('walk_up'):
                return None
            if input_component.is_action_pressed('roll'):
                return Roll()
            if input_component.is_action_pressed('fire'):
                return Fire()
            return self
        return self

    def enter(self, player):
        player.facing = player.UP
        player.current_state = 'walk'
        player.set_animation(player.current_state, player.facing)
        player.speed = (0, -player.walk_speed)


class WalkDown(State):
    def __init__(self):
        State.__init__(self)

    def handle_input(self, input_component):
        if pygame.time.get_ticks() - self.start_time > 64:
            if not input_component.is_action_pressed('walk_down'):
                return None
            if input_component.is_action_pressed('roll'):
                return Roll()
            if input_component.is_action_pressed('fire'):
                return Fire()
            return self
        return self

    def enter(self, player):
        player.facing = player.DOWN
        player.current_state = 'walk'
        player.set_animation(player.current_state, player.facing)
        player.speed = (0, player.walk_speed)


class Roll(State):
    def __init__(self):
        State.__init__(self)

    def enter(self, player):
        player.current_state = 'roll'
        player.set_animation(player.current_state, player.facing)
        player.speed = (0, 0)
        if player.facing == player.UP:
            player.speed = (0, -player.roll_speed)
        elif player.facing == player.RIGHT:
            player.speed = (player.roll_speed, 0)
        elif player.facing == player.DOWN:
            player.speed = (0, player.roll_speed)
        elif player.facing == player.LEFT:
            player.speed = (-player.roll_speed, 0)
