import os

import pygame
import pyganim
from pygame.sprite import groupcollide

from ..AnimatedSprite import AnimatedSprite


class ProjectileGroup(pygame.sprite.Group):

    SPRITE_PATH = os.path.join('project', 'res', 'assets', 'sprites', 'projectiles')

    def __init__(self, debug=False):
        pygame.sprite.Group.__init__(self)
        self.debug = debug
        self.projectile_frames = None
        self.explosion_frames = None
        self.clock = pygame.time.Clock()
        self.explosions = pygame.sprite.Group()
        self.init()

    def init(self):
        # Projectile
        images = pyganim.getImagesFromSpriteSheet(os.path.join(self.SPRITE_PATH, 'Python-logo.png'),
                                                  rows=1, cols=1, rects=[])
        self.projectile_frames = list(zip(images, [100]))

        # Projectile Explosion
        images = pyganim.getImagesFromSpriteSheet(os.path.join(self.SPRITE_PATH, 'explosion.png'),
                                                  rows=2, cols=8, rects=[])
        self.explosion_frames = list(zip(images, [25] * 16))

    def spawn_projectile(self, direction, position):
        if len(self.sprites()) <= 2:
            projectile = Projectile(self.projectile_frames, self.explosion_frames, direction, position, self.debug)
            self.add(projectile)
            return projectile

    def remove_internal(self, sprite):
        try:
            del self.projectiles[sprite]
        except Exception:  # NOQA
            pass
        pygame.sprite.Group.remove_internal(self, sprite)

    def draw(self, surface):
        surface_rect = surface.get_rect()
        for sprite in self.sprites():
            # Kill if off screen
            if sprite.rect.centerx < surface_rect.left - 25 or \
                    sprite.rect.centerx > surface.get_rect().right + 25 or \
                    sprite.rect.centery < surface_rect.top - 25 or \
                    sprite.rect.centery > surface_rect.bottom + 25:
                sprite.kill()
            else:
                sprite.draw(surface)

    def handle_input(self, input_component):
        for sprite in self.sprites():
            sprite.handle_input(input_component)

    def handle_collision(self, sprite_group):
        collisions = groupcollide(self, sprite_group, False, False)
        if collisions:
            if self.debug:
                print(f"Collisions: {collisions}")
            for projectile in collisions:
                projectile.handle_collision(collisions[projectile])


class Projectile(AnimatedSprite):

    PROJECTILE_DAMAGE = 10

    def __init__(self, projectile_frames, explosion_frames, direction, position=None, debug=False):
        AnimatedSprite.__init__(self, position, debug)
        self.debug = debug
        self.projectile = pyganim.PygAnimation(projectile_frames)
        self.explosion = pyganim.PygAnimation(explosion_frames, loop=False)
        self.direction = direction
        self.explode = False
        self.damage = self.PROJECTILE_DAMAGE
        self.current_animation = None
        self.mouse_position = (0, 0)
        self.speed = (10, 10)
        self.input_configured = False
        self.time = pygame.time
        self.start_time = self.time.get_ticks()
        self.countdown = 10000  # Timer in ms
        self.has_exploded = False
        self.state = []
        self.init_state()

        position = position if position else [0, 0]
        size = (self.current_animation.getRect().width,
                self.current_animation.getRect().height)
        self.rect = pygame.Rect(position, size)

    def update(self):
        # Update position
        if self.direction == 'right':
            self.rect.centerx += self.speed[0]
        if self.direction == 'down':
            self.rect.centery += self.speed[1]
        if self.direction == 'left':
            self.rect.centerx -= self.speed[0]
        if self.direction == 'up':
            self.rect.centery -= self.speed[1]

    def handle_input(self, input_component):
        state = self.state[-1].handle_input(input_component)
        if state and state is not self.state[-1]:
            self.state.append(state)
            state.enter(self)
            if self.debug:
                print(f"Projectile State Stack: {self.state}")
        elif not state:
            self.pop_state()

    def handle_collision(self, collisions):
        # Handle collisions passed to us
        if collisions:
            for obj in collisions:
                if isinstance(obj, AnimatedSprite):
                    obj.take_damage(self.damage)
            self.kill()

    def explode(self):
        state = ExplosionState()
        self.state.append(state)
        state.enter(self)

    def init_state(self):
        state = ProjectileState()
        self.state.append(state)
        state.enter(self)


class State:
    def __init__(self):
        self.start_time = pygame.time.get_ticks()

    def handle_input(self, input_component):
        return self

    def enter(self, player):
        pass

    def exit(self, player):
        pass


class ProjectileState(State):
    def __init__(self):
        State.__init__(self)

    def handle_input(self, input_component):
        if input_component.is_action_pressed('explode'):
            return ExplosionState()
        return self

    def enter(self, player):
        player.current_animation = player.projectile
        player.current_animation.play()


class ExplosionState(State):
    def __init__(self):
        State.__init__(self)

    def handle_input(self, input_component):
        return self

    def enter(self, player):
        player.speed = (0, 0)
        player.has_exploded = True
        player.current_animation = player.explosion
        player.current_animation.play()

    def exit(self, player):
        player.kill()
