from .GameWorld import World


def start():
    World().run_game()


if __name__ == '__main__':
    start()
