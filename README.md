# Code Jam 3

In a survery we recently conducted, we asked what theme people wanted to see for this code jam. The most popular option, 
with 26/115 votes, was **The world ends in 10 minutes**. Thus, the task for this code jam involves creating a game under that theme.

This task is a little more freeform than the other code jams we've had so far - we're happy to accept games in any form, 
*as long as they fit that theme.* You may use PyGame, Kivy, Curses/Unicurses, Pyxel, web frameworks like Flask, or anything else 
your team desires. **Please provide instructions on how to set up and run your game within the README where necessary**.

Remember that teamwork is paramount - You *will need to work together*. For this jam, we've assigned a leader for each team 
based on their responses to the application form. Remember to listen to your leader, and communicate with the rest of your team!

# Tips

* Please lint your code, and listen to the linter. We recommend **flake8**, and you can use `pipenv run lint` to run it.
* Remember to work closely with the rest of your team.
* We're not expecting the next Assassins Creed or God of War. Don't get too ambitious and overstretch yourselves - 
  it's much better to have a finished project.
* Remember to fill out the __Project Information__ section below.
* If you haven't used Git before or have other questions about the task or code jam, we recommend reading 
  [the Code Jam section on our wiki](https://wiki.pythondiscord.com/wiki/jams).

# Setting Up

As usual, you should be using [Pipenv](https://pipenv.readthedocs.io/en/latest/). Take a look 
[at the documentation](https://pipenv.readthedocs.io/en/latest/) if you've never used it before. In short:

* Setting up for development: `pipenv install --dev`
* Running the application (assuming you use our project layout): `pipenv run start`

# Project Information

## Installation

`pipenv install`

## Project Highlights:

### GameLoop with Fixed Update Time Step

This allows us to run our game with an uncapped framerate while providing a consistent simulation across both slow and fast systems.

### Input Binding System

We've implemented an input binding system that allows the user to customize the controls they use to play the game. You don't like having to use the mouse to fire projectiles? No worries! You can map the 'fire' action binding to your favorite keyboard key!

### Original Sound Effects

Our intro dialogue and all of our sound effects are original and created by our team for the game. The only exception to this is the background music that's playing, which was open sourced.

### Dynamically Generated Textures

Our game implements a system for dynamically generating our room textures at run time so that the rooms don't look the same from one play to the next. This system also helps lay the ground work for providing procedural generated content in future iterations of our game.

## Description

Threat Level Midnight places the player in a dungeon from which they must survive an onslaught of monsters for 10 minutes in order to survive.

## How to run

`pipenv run start`

## How to play

Game Controls are as follows:

Movement:   WASD or Arrow keys

Roll:       Spacebar

Fire:       Left Mouse Button

Exit:       Escape Key

Note that our game implements a key binding system that allows the player to customize their controls. We didn't have an oppurtunity to implement a UI for it, however it can still be used from within GameWorld.py by adding keyboard/mouse constants to the appropriate action bindings.

Example:
```# UI Bindings
        self.input_component.bind_action('ui_cancel', QUIT)
        self.input_component.bind_action('ui_cancel', K_ESCAPE)
        self.input_component.set_callback('ui_cancel', self.on_quit_game)

        # Movement Bindings
        self.input_component.bind_action('walk_left', K_a)
        self.input_component.bind_action('walk_left', K_LEFT)
        self.input_component.bind_action('walk_down', K_s)
        self.input_component.bind_action('walk_down', K_DOWN)
        self.input_component.bind_action('walk_right', K_d)
        self.input_component.bind_action('walk_right', K_RIGHT)
        self.input_component.bind_action('walk_up', K_w)
        self.input_component.bind_action('walk_up', K_UP)
        self.input_component.bind_action('fire', LEFT_MOUSE)
        # self.input_component.bind_action('explode', RIGHT_MOUSE)
        self.input_component.bind_action('roll', K_SPACE)```
